export interface IImigrasiResponse {
  identityNumber: string;
  allowedFly: boolean;
  reason: string;
}
