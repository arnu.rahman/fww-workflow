export interface IMailDecline {
  reservationStatus: string;
  reason: string;
  passengerMail: string;
  flightCode: string;
  flightDate: string;
  airplane: string;
  deparAirport: string;
  deparTime: string;
  destiAirport: string;
  destiTime: string;
  seatNumber: string;
}
