export interface IDisdukcapilRequest {
  identityNumber: string;
  name: string;
  birthDate: string;
}
