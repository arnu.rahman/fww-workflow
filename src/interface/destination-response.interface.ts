export interface IDestinationResponse {
  statusRule: string;
  discountDestination: number;
}
