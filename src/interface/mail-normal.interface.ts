export interface IMailNormal {
  passengerMail: string;
  flightCode: string;
  flightDate: string;
  airplane: string;
  boardingTime: string;
  deparAirport: string;
  deparTime: string;
  destiAirport: string;
  destiTime: string;
  seatNumber: string;
  typeCode: string;
  reservationCode?: string;
  nextProcess: string;
  deadline: string;
  consequence: string;
  specialNote: string;
  realCode?: string;
}
