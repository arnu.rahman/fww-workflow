import { ESeatStatus } from 'src/core/seat-status.enum';

export interface ISeatStatusUpdate {
  flightDate: string;
  flight: number;
  seat: number;
  seatStatus: ESeatStatus;
}
