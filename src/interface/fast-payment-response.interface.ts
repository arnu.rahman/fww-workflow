export interface IFastPaymentResponse {
  statusRule: string;
  discountFastPayment: number;
}
