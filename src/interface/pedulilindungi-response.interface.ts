export interface IPeduliLindungiResponse {
  identityNumber: string;
  isCovid: boolean;
  vaksinCovid: number;
  shouldPCR: boolean;
}
