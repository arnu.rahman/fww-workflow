import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import {
  IAxiosBaseError,
  IAxiosError,
} from 'src/interface/axios-error.interface';
import { ICheckPaymentResponse } from 'src/interface/check-payment-response.interface';

@Injectable()
export class BookingService {
  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  private readonly logger = new Logger(BookingService.name);
  readonly baseUrl = `http://${this.configService.get<string>(
    'BOOKING_HOST',
  )}:${this.configService.get<string>('BOOKING_PORT')}/api`;
  private readonly auth = {
    username: this.configService.get<string>('BASIC_USER'),
    password: this.configService.get<string>('BASIC_PASS_UNHASH'),
  };

  async checkPayment(paymentId: string) {
    const url = `${this.baseUrl}/v1/check-payment/${paymentId}`;

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.get(url, { auth: this.auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch result status payment from booking engine');
    return data as ICheckPaymentResponse;
  }
}
