import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { catchError, lastValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import {
  IAxiosBaseError,
  IAxiosError,
} from 'src/interface/axios-error.interface';
import { IDisdukcapilRequest } from 'src/interface/disdukcapil-request.interface';
import { IDisdukcapilResponse } from 'src/interface/disdukcapil-response.interface';
import { IImigrasiResponse } from 'src/interface/imigrasi-response.interface';
import { IImigrasiRequest } from 'src/interface/imigrasi-request.interface';
import { IPeduliLindungiRequest } from 'src/interface/pedulilindungi-request.interface';
import { IPeduliLindungiResponse } from 'src/interface/pedulilindungi-response.interface';

@Injectable()
export class RegulationService {
  constructor(
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  private readonly logger = new Logger(RegulationService.name);
  private readonly baseUrl = `http://${this.configService.get<string>(
    'REGULATION_HOST',
  )}:${this.configService.get<string>('REGULATION_PORT')}/api`;

  private readonly auth = {
    username: this.configService.get<string>('BASIC_USER'),
    password: this.configService.get<string>('BASIC_PASS_UNHASH'),
  };

  async checkDisdukcapil(check: IDisdukcapilRequest) {
    const url = `${this.baseUrl}/v1/check-disdukcapil`;
    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.post(url, check, { auth: this.auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch result check disdukcapil');
    return data as IDisdukcapilResponse;
  }

  async checkImigration(check: IImigrasiRequest) {
    const url = `${this.baseUrl}/v1/check-imigrasi`;

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.post(url, check, { auth: this.auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch result check imigrasi');
    return data as IImigrasiResponse;
  }

  async checkPeduliLindungi(check: IPeduliLindungiRequest) {
    const url = `${this.baseUrl}/v1/check-peduli-lindungi`;

    const {
      data: { data },
    } = await lastValueFrom(
      this.httpService.post(url, check, { auth: this.auth }).pipe(
        catchError((error: AxiosError) => {
          const axiosBaseError = error.response.data as IAxiosBaseError;
          const axiosError: IAxiosError = { response: axiosBaseError };
          throw axiosError;
        }),
      ),
    );

    this.logger.log('Fetch result check peduli lindungi');
    return data as IPeduliLindungiResponse;
  }
}
