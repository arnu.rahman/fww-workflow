import { NestFactory } from '@nestjs/core';
import { Logger } from 'nestjs-pino';
import { AppModule } from './app.module';
import helmet from 'helmet';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AppService } from './service/app.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.enableCors();
  app.use(helmet.hidePoweredBy());

  const logger = app.get(Logger);
  app.useLogger(logger);

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidUnknownValues: true,
      transform: true,
      validateCustomDecorators: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );

  const configService = app.get(ConfigService);
  const serviceName = configService.get<string>('APP_NAME');
  const port = configService.get<number>('APP_PORT');
  await app.listen(port);
  logger.log(`${serviceName} is running on port ${port}`);

  const appService = app.get(AppService);
  await appService.subscribeWorkers();
}

bootstrap().then(null).catch(console.error);
